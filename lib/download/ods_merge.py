#!/usr/bin/env python3
"""
    Merge OpenDataSoft (ODS) dumps into 2 csv for importing into db 
"""
import argparse
import csv
import logging
import re
import urllib
from pathlib import Path

import requests
import ujson as json

LOG = logging.Logger('ods_download')

ODS_CSV_RE = re.compile(r'^([0-9]+)_([0-9a-z_-]+)_(catalog|monitoring)$')


def compute_common_header_set(csv_files):
    """ Extracts headers in each csv files and returns common header """

    common_header = None
    for csv_file in csv_files:

        with csv_file.open('rt', encoding='utf-8') as fdin:
            csv_reader = csv.reader(fdin, delimiter=',')
            for row in csv_reader:
                header = set(row)
                if len(header) == 0:
                    break
                if common_header is None:
                    common_header = header
                else:
                    common_header = common_header & header
                break

    return common_header


def csv_merge(merged_file: Path, csv_files):
    """ Reads CSV files and merges them in another one """

    # Compute common headers found in csv files
    # adding additional '_siren' info
    headers = ['_siren'] + sorted(compute_common_header_set(csv_files))

    # Writes into a unique CSV file
    with merged_file.open('wt', encoding='utf-8') as fdout:
        csv_writer = csv.writer(fdout, delimiter=',')

        csv_writer.writerow(headers)

        for csv_file in csv_files:
            # _siren and _nom are added for each CSV line
            # to ease further filtering
            m = ODS_CSV_RE.match(csv_file.stem)
            if not m:
                print('Ignoring file {}...'.format(csv_file.name))
                continue
            siren = m.group(1)
            with csv_file.open('rt', encoding='utf-8') as fdin:
                csv_reader = csv.DictReader(fdin, delimiter=',')
                for row in csv_reader:
                    row['_siren'] = siren
                    data = [row[h] for h in headers]
                    csv_writer.writerow(data)


def main():
    """ Reads OpenDataFrance CSV files and merge them """
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('ods_dumps_dir', type=Path, help='OpenDataSoft dumps dir')
    parser.add_argument('target_dir', type=Path, help='Target dir for ods merged CSV files')
    args = parser.parse_args()

    if not args.ods_dumps_dir.exists():
        parser.error("Can't find [{}] CSV input dir".format(str(args.ods_dumps_dir)))
    csv_dir = args.ods_dumps_dir
    if not args.target_dir.exists():
        parser.error("Can't find [{}] target dir".format(str(args.target_dir)))
    target_dir = args.target_dir

    catalog_csv_file = target_dir / 'ods_catalog.csv'
    csv_merge(catalog_csv_file, [f for f in csv_dir.glob("*_catalog.csv")])
    monitoring_csv_file = target_dir / 'ods_monitoring.csv'
    csv_merge(monitoring_csv_file, [f for f in csv_dir.glob("*_monitoring.csv")])


if __name__ == '__main__':
    main()
