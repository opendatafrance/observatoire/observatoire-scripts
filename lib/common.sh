#!/bin/bash
#
# Common initialization
#
set -euo pipefail

if [ "$0" == "common.sh" ]; then
    echo "This script is not intended to be called directly"
    exit 1
fi

# caller script filepath
SCRIPT=$(readlink -f "$0")

# Project base DIR
BASE_DIR=$(dirname "$SCRIPT")

# Other dirs
BUILD_DIR=$BASE_DIR/build
CACHE_DIR=$BASE_DIR/cache
DUMPS_DIR=$BASE_DIR/dumps
LIB_DIR=$BASE_DIR/lib
RSC_DIR=$BASE_DIR/rsc

# Pivot directories
DG_DUMP_DIR=$DUMPS_DIR/datagouv
ODS_DUMP_DIR=$DUMPS_DIR/opendatasoft

# Pivot files
ODF_ORGA_FILE=$DUMPS_DIR/opendatafrance/organisations.csv
ODF_PTF_FILE=$DUMPS_DIR/opendatafrance/plateformes.csv

# Legacy
SQLITE_DB=$CACHE_DIR/process.db
SQLITE_GEOREF_DB=$CACHE_DIR/georef.db


# Tools
CSV_OPTS="-z 1000000"
CSV_CUT="csvcut $CSV_OPTS"
CSV_FORMAT="csvformat $CSV_OPTS"
CSV_GREP="csvgrep $CSV_OPTS"
CSV_JOIN="csvjoin $CSV_OPTS"
CSV_SORT="csvsort $CSV_OPTS"
GREP=grep
ICONV=icon
PYTHON=python3
SED=sed
SQLITE3=sqlite3

# Misc
download_file_or_exit() {
    url=$1
    outfile=$2
    
    wget -q $url -O $outfile
    if [ "$?" != "0" ]; then
        echo "Error during download of $url, stopping."
        exit 1
    fi
}