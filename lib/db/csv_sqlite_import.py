#!/usr/bin/env python3
"""
    Imports a CSV file into an existing SQLite DB
"""
import argparse
import collections
import csv
import logging
import re
import sqlite3
import sys
import unicodedata
from collections import OrderedDict
from pathlib import Path
from timeit import default_timer as timer
from typing import List

NORMALIZE_RE = re.compile("['\"\\. -]+")

DELIMITER_NAME_TO_CHAR = collections.OrderedDict(
    [
        ("comma", ","),
        ("semicolon", ";"),
        ("tab", "\t"),
        ("pipe", "|"),
    ]
)

log = logging.getLogger(__name__)


class SqliteTableHelper:
    """Ease SQLite table handling"""

    def __init__(self, cur, table_name, headers, col_type):
        """Inits instance"""
        self.cur = cur
        self.table_name = table_name
        self.headers = headers
        self.col_type = col_type
        self._inserted_rows_nb = 0

        # Computes SQL INSERT statement
        col_names = ["'{}'".format(h["name"]) for h in headers]
        q_list = ["?"] * len(headers)
        self.sql_insert = "INSERT INTO {} ({}) VALUES ({});".format(
            table_name, ", ".join(col_names), ", ".join(q_list)
        )

    def create_table(self):
        """Creates table"""
        self.cur.execute("DROP TABLE IF EXISTS {}".format(self.table_name))
        table_constraints = [
            "{} {}".format(h["name"], self.col_type.get(i, "TEXT"))
            for i, h in enumerate(self.headers)
        ]
        sql_tpl = "CREATE TABLE {} ({})".format(
            self.table_name, ", ".join(table_constraints)
        )
        log.debug(sql_tpl)
        self.cur.execute(sql_tpl)

    def flush_insert(self, data_buffer, doit):
        """Inserts data into table if any. Reset data_buffer"""
        if not data_buffer or not doit:
            return
        try:
            self.cur.executemany(self.sql_insert, data_buffer)
        except sqlite3.ProgrammingError:
            log.exception()
            for i, row in enumerate(data_buffer):
                log.debug("#%d (%d) %r", i, len(row), row)
        self._inserted_rows_nb += len(data_buffer)
        data_buffer.clear()

    def inserted_rows_nb(self):
        """Returns nb of inserted rows"""
        return self._inserted_rows_nb


def norm_string(s):
    """Change 'Hélène à côté' into 'helene_a_cote'"""
    s = "".join(
        c
        for c in unicodedata.normalize("NFD", s.lower())
        if unicodedata.category(c) != "Mn"
    )
    s = s.replace("°", "o")
    return NORMALIZE_RE.sub("_", s)


def norm_table_name(table_name):
    """Change ODATER_DATA_ORGA_AVRIL18 into odater_data_orga_avril18"""
    return norm_string(table_name)


def norm_col_name(col: str, idx: int):
    """Change 'metric.members' into 'metrics_members'.

    Return 'unnamed_{idx}' if col is empty
    """
    norm_col = norm_string(col)
    return norm_col if norm_col else f"unnamed_{idx}"


def import_csv(csv_filepath: Path, table_name, conn, csv_delimiter, col_type):
    """Converts CSV file"""
    if not csv_filepath.exists():
        sys.stderr.write("CSV file [{}] not found!\n".format(str(csv_filepath)))
        return

    def type_row_values(row, col_type, row_id, headers):
        """Converts some row values from string to INTEGER or REAL"""
        if not col_type:
            return row

        typed_row = []
        for i, val in enumerate(row):
            if i in col_type:
                if col_type[i] == "INTEGER":
                    try:
                        val = int(val) if val != "" else 0
                    except ValueError:
                        log.warning(
                            "Line %d: %s: Incorrect integer value %r",
                            row_id,
                            headers[i],
                            val,
                        )
                        val = 0

                elif col_type == "REAL":
                    try:
                        val = float(val) if val != "" else 0.0
                    except ValueError:
                        log.error(
                            "Line %d: %d: Incorrect float value %r",
                            row_id,
                            headers[i],
                            val,
                        )
                        val = 0.0

            typed_row.append(val)
        return typed_row

    log.info("Importing %s into table %s...", str(csv_filepath), table_name)

    cur = conn.cursor()
    cur.execute("BEGIN TRANSACTION")
    row_buffer: List[str] = []

    sql_batch_rows_nb = 100
    sth = None

    # Reads CSV line by line
    with csv_filepath.open(mode="rt", encoding="utf-8") as csv_fd:
        reader = csv.reader(csv_fd, delimiter=csv_delimiter)
        header = None
        for cp, row in enumerate(reader):

            # Header line
            if header is None:
                header = row

                # row headers
                headers = [
                    {"orig": col, "name": norm_col_name(col, i + 1)}
                    for i, col in enumerate(header)
                ]

                # Display info on typed cols!!!
                if col_type:
                    for index, sqltype in col_type.items():
                        if index >= len(header):
                            log.error(
                                "  Unknown column #%d to treat as %s, stopping here.",
                                index,
                                sqltype,
                            )
                            sys.exit(1)
                        else:
                            log.debug(
                                "  Column %s is treated as %s", header[index], sqltype
                            )

                # Creates table
                sth = SqliteTableHelper(cur, table_name, headers, col_type)
                sth.create_table()
                continue

            # 'normal' row
            if len(row) == len(header):
                row_buffer.append(type_row_values(row, col_type, cp, header))
            else:
                log.warning("Bad number of columns on row #%d, ignoring data", cp)

            # Flush?
            sth.flush_insert(row_buffer, cp != 0 and cp % sql_batch_rows_nb == 0)

    # Flush last buffered row values
    if sth:
        sth.flush_insert(row_buffer, True)
    conn.commit()

    return sth.inserted_rows_nb() if sth else 0


def main():
    """Converts CSV files to SQLite"""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("csv_file", type=Path, help="CSV file to import")
    parser.add_argument(
        "db_file", type=Path, help="SQLite DB file to create or populate"
    )
    parser.add_argument(
        "--csv_delimiter",
        help="CSV delimiter",
        choices=DELIMITER_NAME_TO_CHAR.keys(),
        default="comma",
    )
    parser.add_argument(
        "--table_name", help="SQL table name (otherwise use CSV file name)"
    )
    parser.add_argument(
        "--integer_cols",
        help='Indexes (starting at 0 and separated by ",") of columns containing integer data',
    )
    parser.add_argument(
        "--float_cols",
        help='Indexes (starting at 0 and separated by ",") of columns containing float data',
    )
    parser.add_argument("--log", default="INFO", help="level of logging messages")
    args = parser.parse_args()

    # Logging settings
    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,  # Use stderr if script outputs data to stdout.
    )

    # To avoid "field larger than field limit (131072)" error
    csv.field_size_limit(sys.maxsize)

    if not args.csv_file.exists():
        parser.error("CSV file [{}] not found".format(str(args.csv_file)))

    # DB connection
    db_file_name = str(args.db_file)
    log.info("Populating %s database", db_file_name)
    conn = sqlite3.connect(db_file_name)

    # Table name overloading
    table_name = norm_table_name(args.csv_file.stem)
    if args.table_name:
        table_name = args.table_name

    # CSV delimiter
    csv_delimiter = DELIMITER_NAME_TO_CHAR[args.csv_delimiter]

    col_type = OrderedDict()

    # Integer colum indexes
    if args.integer_cols:
        for index in args.integer_cols.split(","):
            col_type[int(index)] = "INTEGER"
    # Float column indexes
    if args.float_cols:
        for ind_str in args.float_cols.split(","):
            index = int(ind_str)
            if index in col_type:
                parser.error(
                    "Column #{} can't be INTEGER and REAL at the same time".format(
                        index
                    )
                )
            col_type[index] = "REAL"

    start = timer()
    rows_nb = import_csv(args.csv_file, table_name, conn, csv_delimiter, col_type)
    end = timer()

    # Close
    conn.close()
    log.info("Done (%d rows imported in %.2fs).", rows_nb, end - start)


if __name__ == "__main__":
    main()
