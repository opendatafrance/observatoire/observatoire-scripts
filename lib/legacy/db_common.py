#!/usr/bin/env python3
"""
    Some common information shared by several scripts
"""

# Tables and field names reference
SQL_CONSTANTS = {
    'orga_metrics_table': 'orga_metrics',
    'orga_metrics_siren_col': 'siren',
    'orga_metrics_source_col': 'source',
    'orga_metrics_met_type_col': 'met_type',
    'orga_metrics_met_value_col': 'met_value',

    'orga_geometry_table': 'orga_geometry',
    'orga_geometry_code_col': 'code',
    'orga_geometry_coords_col': 'coords',
    'orga_geometry_nom_col': 'nom',
    'orga_geometry_siren_col': 'siren',
    'orga_geometry_source_col': 'source',

    'ref_org_table': 'ref_org',
    'ref_org_siren_col': 'siren',
    'ref_org_name_col': 'nom',
    'ref_org_type_col': 'type',
    'ref_org_dg_org_id_col': 'id_datagouv',
    'ref_org_ods_org_id_col': 'id_ods',

    'dg_org_table': 'data_gouv_organizations',
    'dg_org_id_col': 'id',
    'dg_org_siren_col': '_siren',
    'dg_org_url_col': 'url',

    'dg_ds_table': 'data_gouv_datasets',
    'dg_ds_org_id_col': 'organization_id',
    'dg_ds_siren_col': '_siren',
    'dg_ds_metric_views_col': 'metric_views',
    'dg_ds_license_col': 'license',

    'dg_res_table': 'data_gouv_resources',
    'dg_res_org_id_col': 'dataset_organization_id',
    'dg_res_siren_col': '_siren',
    'dg_res_downloads_col': 'downloads',
    'dg_res_filesize_col': 'filesize',
    'dg_res_format_col': 'format',
    'dg_res_created_at_col': 'created_at',
    'dg_res_modified_col': 'modified',

    'odf_org_table': 'data_orga',
    'odf_org_siren_col': 'siren',
    'odf_org_nom_col': 'nom',
    'odf_org_regcode_col': 'regcode',
    'odf_org_depcode_col': 'depcode',
    'odf_org_dg_org_id_col': 'id_datagouv',
    'odf_org_dg_url_col': 'url_datagouv',
    'odf_org_ptf_url_col': 'url_ptf',
    'odf_org_ptf_nb_col': 'nb_ptf',
    'odf_org_ods_org_id_col': 'id_ods',
    'odf_org_type_col': 'type',
    'odf_org_lat_col': 'lat',
    'odf_org_long_col': 'long',

    'ods_catalog_table': 'ods_catalog',
    'ods_catalog_license_col': 'default_license',
    'ods_catalog_def_modified_col': 'default_modified',
    'ods_catalog_def_metadata_processed_col': 'default_metadata_processed',
    'ods_monitoring_table': 'ods_monitoring',
    'ods_monitoring_download_count_col': 'download_count',
    'ods_monitoring_records_count_col': 'records_count',
}
