#!/usr/bin/env python3
"""
    Creates a table orga_metrics in the db
	and fills it with information harvester from different provider

    Warning: If you add a new provider, don't forget to update
        fill_opendatasoft metrics function WHERE_CRITERIA string
"""
import argparse
from pathlib import Path
import sqlite3
import db_common as dc
import metrics_common as mc


def add_indexes_to_datagouv_tables(cursor):
    """ Adds indexes to datagouv tables to speed up UPDATE queries """
    # TODO: Index SIREN now?
    cursor.execute("""CREATE INDEX IF NOT EXISTS {dg_ds_table}_{dg_ds_org_id_col}_idx
                        ON {dg_ds_table}({dg_ds_org_id_col})""".format(**dc.SQL_CONSTANTS))
    cursor.execute("""CREATE INDEX IF NOT EXISTS {dg_res_table}_{dg_res_org_id_col}_idx
                        ON {dg_res_table}({dg_res_org_id_col})""".format(**dc.SQL_CONSTANTS))


def fill_datagouv_metrics(cursor):
    """ Add datagouv metrics:
            - number of datasets per organization
            - number of cumulated views (dataset level)
            - license type list
            - resource nb
            - downloads nb
            - resource types
    """

    # First flush all datagouv metrics
    reset_metrics(cursor, mc.DATA_GOUV_TYPE)

    # URL
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT {odf_org_siren_col}, ?, ?, {odf_org_dg_url_col}
             FROM {odf_org_table}
             WHERE {odf_org_dg_org_id_col} != ''
             GROUP BY {odf_org_siren_col}"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.DATA_GOUV_TYPE, 'url'))

    # Dataset nb
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, COUNT(*) FROM {dg_ds_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.DATA_GOUV_TYPE, 'dataset_nb'))

    # Cumulated views
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, SUM({dg_ds_metric_views_col}) FROM {dg_ds_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.DATA_GOUV_TYPE, 'cumulated_views'))

    # License type
    # Note: this REPLACE(TRIM(REPLACE...)) is over complicated because
    # the simple GROUP_CONCAT(DISTINCT col, '|') produces a syntax error on SQLite
    # (GROUP_CONCAT(col, '|') doesn't...
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?,
                REPLACE(TRIM(REPLACE(GROUP_CONCAT(DISTINCT {dg_ds_license_col}), ',', '|'),'|'),'||','|')
             FROM {dg_ds_table}
             GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.DATA_GOUV_TYPE, 'license_types'))

    # Resource nb
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, COUNT(*) FROM {dg_res_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.DATA_GOUV_TYPE, 'resource_nb'))

    # Downloads nb
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, SUM({dg_res_downloads_col}) FROM {dg_res_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.DATA_GOUV_TYPE, 'downloads_nb'))

    # First created
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, REPLACE(MIN({dg_res_created_at_col}), 'T', ' ')
             FROM {dg_res_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.DATA_GOUV_TYPE, 'first_created'))

    # Last modified
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, REPLACE(MAX({dg_res_modified_col}), 'T', ' ')
             FROM {dg_res_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.DATA_GOUV_TYPE, 'last_modified'))

    # Resource types
    # Same trick for GROUP_CONCAT(DISTINCT, '|')
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?,
                REPLACE(TRIM(REPLACE(GROUP_CONCAT(DISTINCT {dg_res_format_col}), ',', '|'),'|'),'||','|')
             FROM {dg_res_table}
             GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.DATA_GOUV_TYPE, 'resource_types'))


def fill_opendatasoft_metrics(cursor):
    """ Add opendatasoft metrics:
            - URL
            - number of datasets per organization
            - license type list
            - resource nb
            - downloads nb
            - first created
            - last modified
    """

    # First flush all opendatasoft metrics
    reset_metrics(cursor, mc.OPENDATASOFT_TYPE)

    # URL is found in 'url_ptf'
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT {odf_org_siren_col}, ?, ?, {odf_org_ptf_url_col}
             FROM {odf_org_table}
             WHERE {odf_org_ods_org_id_col} != ''
          """
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.OPENDATASOFT_TYPE, 'url'))

    # Dataset nb
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, COUNT(*) FROM {ods_catalog_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.OPENDATASOFT_TYPE, 'dataset_nb'))

    # No cumulated views

    # License type
    # Note: this REPLACE(TRIM(REPLACE...)) is over complicated because
    # the simple GROUP_CONCAT(DISTINCT col, '|') produces a syntax error on SQLite
    # (GROUP_CONCAT(col, '|') doesn't...
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?,
                REPLACE(TRIM(REPLACE(GROUP_CONCAT(DISTINCT {ods_catalog_license_col}), ',', '|'),'|'),'||','|')
             FROM {ods_catalog_table}
             GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.OPENDATASOFT_TYPE, 'license_types'))

    # Resource nb
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, SUM({ods_monitoring_records_count_col})
             FROM {ods_monitoring_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.OPENDATASOFT_TYPE, 'resource_nb'))

    # Downloads nb
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, SUM({ods_monitoring_download_count_col})
             FROM {ods_monitoring_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.OPENDATASOFT_TYPE, 'downloads_nb'))

    # First created
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, MIN({ods_catalog_def_modified_col})
             FROM {ods_catalog_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.OPENDATASOFT_TYPE, 'first_created'))

    # Last modified
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT _siren, ?, ?, MAX({ods_catalog_def_metadata_processed_col})
             FROM {ods_catalog_table} GROUP BY _siren"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.OPENDATASOFT_TYPE, 'last_modified'))

    # No resource types


def fill_opendatafrance_metrics(cursor):
    """ Add opendatafrance metrics:
            - URL
            - number of datasets per organization
    """

    # First flush all opendatafrance metrics
    reset_metrics(cursor, mc.OPENDATAFRANCE_TYPE)

    # IMPORTANT INFORMATION
    # This WHERE criteria has to be updated if another harvest platform is added
    WHERE_CRITERIA = "WHERE {odf_org_ods_org_id_col} == '' AND {odf_org_ptf_url_col} != ''"

    # URL
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT {odf_org_siren_col}, ?, ?, {odf_org_ptf_url_col}
             FROM {odf_org_table}
          """ + WHERE_CRITERIA
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.OPENDATAFRANCE_TYPE, 'url'))

    # Dataset nb
    sql = """INSERT INTO {orga_metrics_table} (siren, source, met_type, met_value)
             SELECT {odf_org_siren_col}, ?, ?, {odf_org_ptf_nb_col}
             FROM {odf_org_table}
          """ + WHERE_CRITERIA
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (mc.OPENDATAFRANCE_TYPE, 'dataset_nb'))


def reset_metrics(cursor, met_type):
    """ Flush all metrics typed from given type """
    sql = "DELETE FROM {orga_metrics_table} WHERE source=?".format(**dc.SQL_CONSTANTS)
    cursor.execute(sql, (met_type,))


def clean_empty_metrics(cursor):
    """ Deletes all empty metrics """
    sql = "DELETE FROM {orga_metrics_table} WHERE met_value='' OR met_value IS NULL".format(**dc.SQL_CONSTANTS)
    cursor.execute(sql)


def build_orga_metrics(cursor):
    """ Creates orga_metrics table structure fills it with metrics """

    # First remove table if exists
    cursor.execute('DROP TABLE IF EXISTS {orga_metrics_table}'.format(**dc.SQL_CONSTANTS))

    # Then fills it with data_orga data
    sql = """CREATE TABLE {orga_metrics_table} (
                siren TEXT,
                source TEXT,
                met_type TEXT,
                met_value TEXT
             )""".format(**dc.SQL_CONSTANTS)
    cursor.execute(sql)

    # Datagouv metrics
    add_indexes_to_datagouv_tables(cursor)
    fill_datagouv_metrics(cursor)

    # OpenDataSoft metrics
    fill_opendatasoft_metrics(cursor)

    # OpenDataFrance (minimal) metrics
    fill_opendatafrance_metrics(cursor)

    # Remove empty values
    clean_empty_metrics(cursor)


def main():
    """ Builds metrics table """
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("db_file", type=Path, help="SQLite DB file")

    args = parser.parse_args()

    assert args.db_file.exists()

    conn = sqlite3.connect(str(args.db_file))
    cur = conn.cursor()

    build_orga_metrics(cur)

    conn.commit()
    conn.close()


if __name__ == '__main__':
    main()
