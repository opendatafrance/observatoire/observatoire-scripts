#!/usr/bin/env python3
"""
    Common code to generate metrics information
"""
import json
import db_common as dc

DATA_GOUV_TYPE = 'dg'
OPENDATASOFT_TYPE = 'ods'
OPENDATAFRANCE_TYPE = 'odf'

# Display order in markdown and GeoJSON feature description
PLATFORM_DISPLAY_ORDER = [OPENDATAFRANCE_TYPE, OPENDATASOFT_TYPE, DATA_GOUV_TYPE]

METRICS_MAP = {
    DATA_GOUV_TYPE: 'DataGouv',
    OPENDATASOFT_TYPE: 'OpenDataSoft',
    OPENDATAFRANCE_TYPE: 'Plateforme territoriale',
}


TYPE_MAP = {
    'AGCT': 'Autre groupement de collectivités territoriales',
    'CA': 'Communauté d\'agglomération',
    'CC': 'Communauté de communes',
    'COM': 'Commune',
    'CU': 'Communauté urbaine',
    'DEP': 'Département',
    'DSP': 'Délégataire de service public',
    'DSPT': 'Délégataire de service public',
    'EPT': 'Établissement public territorial',
    'MET': 'Métropole',
    'OACT': 'Organisme associé de collectivité territoriale',
    'REG': 'Région',
}

PLURAL_TYPE_MAP = {
    'AGCT': 'autres groupements de collectivités territoriales',
    'CA': 'communautés d\'agglomération',
    'CC': 'communautés de communes',
    'COM': 'communes',
    'CU': 'communautés urbaines',
    'DEP': 'départements',
    'DSP': 'délégataires de service public',
    'DSPT': 'délégataires de service public',
    'EPT': 'établissements publics territoriaux',
    'MET': 'métropoles',
    'OACT': 'organismes associés de collectivité territoriale',
    'REG': 'régions',
}


def format_list(str_list, bold):
    """ Formats ['json', 'shp'] into `json`, `shp` """
    return ', '.join(map(lambda s: '{0}{1}{0}'.format(bold, s), str_list.split('|')))


def format_date(date_str):
    """ Formats '2014-10-21T16:02:46.832000' into '21/10/2014 à 16h02' """
    if date_str is None:
        return '<info manquante>'

    date_info = {
        'aaaa': date_str[:4],
        'mm':   date_str[5:7],
        'jj':   date_str[8:10],
        'hm':   date_str[11:16].replace(':', 'h')
    }
    return '{jj}/{mm}/{aaaa} à {hm}'.format(**date_info)


def sing_plur(nb, none_form, sing_form, plur_form):
    """ Compute expression with right singular / plural form """
    if nb == 0:
        return none_form
    return '{} {}'.format(nb, sing_form if nb == 1 else plur_form)


def convert_metrics(md_fd, source, met, mode):
    """ Convert metrics info into markdown """

    bold = '`' if mode == 'md' else ''

    if 'url' in met:
        md_fd.write('- URL : {}\n'.format(met['url']))
    dataset_str = sing_plur(met.get('dataset_nb', 0), 'aucun jeu de données',
                            'jeu de données', 'jeux de données')
    resource_str = ''
    if 'resource_nb' in met:
        res_labels = ['aucune ressource', 'ressource', 'ressources']
        if source == 'ods':
            res_labels = ['aucun enregistrement', 'enregistrement', 'enregistrements']
        resource_str = ' et ' + sing_plur(met.get('resource_nb', 0), *res_labels)
    md_fd.write('- Volumétrie : {}{}\n'
                .format(dataset_str, resource_str))
    if 'cumulated_views' in met:
        md_fd.write('- Nombre de vues (tous jeux de données confondus) : {}\n'.format(met['cumulated_views']))
    if 'downloads_nb' in met:
        addon_info = 'toutes ressources confondues' if source != 'ods' else 'tous jeux de données confondus'
        md_fd.write('- Nombre de téléchargements ({}) : {}\n'.format(addon_info, met['downloads_nb']))
    if 'resource_types' in met:
        md_fd.write('- Types de ressources disponibles : {}\n'.format(format_list(met['resource_types'], bold)))
    if 'license_types' in met:
        md_fd.write('- Types de licences utilisées : {}\n'.format(format_list(met['license_types'], bold)))
    if 'first_created' in met:
        md_fd.write('- Date de création : le {}\n'.format(format_date(met['first_created'])))
    if 'last_modified' in met:
        md_fd.write('- Date de dernière mise à jour le {}\n'.format(format_date(met['last_modified'])))
    md_fd.write('\n')


def split_and_sort_if_possible(val):
    """ Splits and sort given val if not None """
    if not val:
        return None
    return sorted(val.split(','))


def compute_org_from_row(row):
    """ Transform SQL row values into dict """
    org = {
        # 'resource.url': row['dg_url'],
        'title': row['nom'],
        'type': row['type'],
        'metrics': row['metrics'],
    }
    return org


def write_md_content(row, fd, with_title=True, mode='md'):
    org = compute_org_from_row(row)

    # Title
    if with_title:
        fd.write("## {}\n\n".format(org['title']))

    # Type
    org_type = org['type']
    fd.write('*{}*\n'.format(TYPE_MAP.get(org_type, org_type)))

    # Metrics
    for source in [metric for metric in PLATFORM_DISPLAY_ORDER if metric in org['metrics']]:
        if mode == 'md':
            fd.write('\n### Source **{}**\n'.format(METRICS_MAP[source]))
        else:
            fd.write('\n**Source {}**\n'.format(METRICS_MAP[source]))

        convert_metrics(fd, source, org['metrics'][source], mode)


def compute_metrics_tree(siren, cursor):
    """ Query orga_metrics table and returns a dict structure """
    sql = """SELECT {orga_metrics_source_col},
                    {orga_metrics_met_type_col},
                    {orga_metrics_met_value_col}
             FROM {orga_metrics_table}
             WHERE {orga_metrics_siren_col} = ?"""
    cursor.execute(sql.format(**dc.SQL_CONSTANTS), (siren,))
    met = {}
    for row in cursor.fetchall():
        src = row['source']
        if src not in met:
            met[src] = {}
        met[src][row['met_type']] = row['met_value']
    return met
