-- Create ref_org handy table
DROP TABLE IF EXISTS ref_org;
CREATE TABLE ref_org AS
SELECT siren, nom, type
FROM data_orga
ORDER BY nom;
