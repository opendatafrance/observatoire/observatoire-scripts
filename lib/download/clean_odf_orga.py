#!/usr/bin/env python3
"""Clean CSV file.

- strip spaces and '\u200e' (Left-to-right mark) in siren column
- de-duplicate siren
- replace comma (,) by dot (.) in lat and lng coordinates
"""
import argparse
import csv
import sys
from pathlib import Path


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("source_csv_file", type=Path, help="source CSV file")
    parser.add_argument("target_csv_file", type=Path, help="target CSV file")
    args = parser.parse_args()

    if not args.source_csv_file.exists():
        parser.error("Source file %r not found", args.source_csv_file)
    source_csv_file = args.source_csv_file

    target_csv_file = args.target_csv_file

    siren_set = set()
    with source_csv_file.open("rt", encoding="utf-8") as fin:
        reader = csv.reader(fin)
        header = next(reader)
        assert header[0] == "siren", header[0]
        assert header[11] == "lat", header[11]
        assert header[12] == "long", header[12]
        with target_csv_file.open("wt", encoding="utf-8") as fout:
            writer = csv.writer(fout)
            writer.writerow(header)

            for row in reader:
                # SIREN cleaning
                siren = row[0].strip("\u200e ")
                if siren in siren_set:
                    print(f"Duplicate siren found: {siren!r}")
                    continue
                siren_set.add(siren)
                row[0] = siren

                # Coords formatting
                row[11] = row[11].replace(",", ".")
                row[12] = row[12].replace(",", ".")

                writer.writerow(row)


if __name__ == "__main__":
    sys.exit(main())
