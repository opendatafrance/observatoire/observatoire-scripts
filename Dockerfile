FROM python:3.9-slim-buster

LABEL maintainer="contact@jailbreak.paris"

RUN apt-get update && apt-get install --yes --no-install-recommends \
    csvkit \
    git \
    openssh-client \
    python3-dev \
    sqlite3 \
    wget

COPY requirements.txt .
RUN pip install --no-cache-dir --requirement requirements.txt
