# Observatoire-scripts

Production et mise à jour d'indicateurs pour l'Observatoire Open Data des territoires

Cette mise à jour s'effectue en 3 étapes :

- récupération de données de référence
- génération d'indicateurs
- déploiement des indicateurs calculés

## Chaîne de production

### Récupération de données de référence

Les données qui permettent de calculer les indicateurs proviennent de 3 sources principales :

- OpenDataFrance
- data.gouv.fr
- OpenDataSoft

L'assocation _OpenDataFrance_ maintient un [tableau Googlesheet](https://docs.google.com/spreadsheets/d/1iNHCRV4mfu4P0-VeiMFV6VVJvqQS8yf0sHNwkVcmGiQ/edit#gid=0) qui recense les organisations françaises qui proposent de l'OpenDATA.
C'est le fichier maître de la chaîne de production des indicateurs.

_data.gouv.fr_ maintient une base de données d'organisations, de datasets et de ressources. Certaines organisations publient leur données ouvertes sur data.gouv.fr mais le site référence également des données ouvertes publiées ailleurs.

_OpenDataSoft_ fournit à ses clients (collectivités territoriales) des portails OpenData.

La récupération de données est assurée par le script [download_and_prepare_data](download_and_prepare_data) :

- téléchargement des données OpenDataFrance :
  - récupération du contenu des onglets `organisations` et `plateformes`
- téléchargement des données data.gouv.fr :
  - récupération des _dumps_ de la base de données (`organisations`, `datasets` et `resources`) et filtrage pour ne retenir que les organisations recensées par OpenDataFrance
- téléchargement des données OpenDataSoft :
  - le script parcourt la liste des organisations recensées par OpenDataFrance et interroge le portail OpenDataSoft correspondant lorsque c'est le cas (colonne `id_ods`).

Les données CSV téléchargées sont placées dans le dossier `dumps`

Par ailleurs, le script utilise le service `entreprise.data.gouv.fr` pour obtenir les données géographiques correspondantes au numéros siren des organisations. De plus les ressources géographiques du dossier `rsc/` sont utilisées pour établir une liste de référence des régions et départements français.

À la fin de l'exécution de ce script, 2 bases de données SQLite sont constituées :

- `cache/process.db` : contient les données d'indicateurs
- `cache/georef.db` : contient les données géographiques calculées à partir des fichiers de référence du dossier `rsc/`

### Calcul et production d'indicateurs

À partir des données téléchargées à l'étape précédente, le script [process_and_generate](process_and_generate) combine et génère des fichiers qui seront sauvegardés dans le projet GitLab [observatoire-data](https://git.opendatafrance.net/observatoire/observatoire-data) :

- des fichiers CSV de référence (à la racine):
  - `organizations.csv`
  - `websites.csv`
  - `datasets-datagouv.csv`
- un fichier markdown de présentation des organisations : `exports/markdown/organisations.md`
- des fichiers GeoJSON à destination de la [carte UMAP](https://umap.openstreetmap.fr/fr/map/odservatoire_256503#6/45.813/5.603) :
  - `exports/odservatoire_agct.json`
  - `exports/odservatoire_com.json`
  - `exports/odservatoire_dep.json`
  - `exports/odservatoire_dspt.json`
  - `exports/odservatoire_epci.json`
  - `exports/odservatoire_oact.json`
  - `exports/odservatoire_reg.json`

Note : les fichiers bruts qui ont servi au calcul sont stockés dans le dossier `sources`

Enfin, une base de données SQLite est générée pour être utilisée par le [tableau de bord de l'observatoire de l'Open DATA](https://lab.observatoire-opendata.fr/superset/dashboard/2/)

### Déploiement des indicateurs calculés

Le déploiement des indicateurs est effectué par la [pipeline CI](.gitlab-ci.yml) après avoir effectué le téléchargement des données et la production des indicateurs.

La pipeline CI gère aussi la reconstruction de l'image de container utilisé par la pipeline en cas de changement dans le `Dockerfile` ou les dépendances

La pipeline est appelé quotidiennement.

## Organisation du projet

- [rsc/](/rsc) : données de référence (voir [rsc/README.md](rsc/README.md) pour plus d'informations)
- [lib/](/lib) : scripts de téléchargement et de traitement de l'information
- Dossiers de travail (contenant notamment les fichiers d'artifacts créés à l'exécution des jobs "Download and generate data", et téléchargeables [depuis l'interface de Gitlab](https://git.opendatafrance.net/observatoire/observatoire-scripts/-/jobs)) :
  - cache/ : dossier de travail
  - dumps/ : données téléchargées (et filtrées pour ne retenir que les données relatives aux organisations recensées par OpenDataFrance)
    - datagouv/
    - opendatafrance/
    - opendatasoft/
  - build/ : données produites
    - observatoire/ : données consolidées
    - geo/ : fichiers geojson
    - markdown/ : page de description des organisations
    - db/ : base de données SQLite pour superset

## Dépendances techniques

Les outils en ligne de commande suivants doivent être installés pour faire fonctionner la chaîne de traitement :

- bash
- csvcut, csvjoin, csvsort (fourni par le package [csvkit](https://csvkit.readthedocs.io/en/latest/))
- grep
- iconv
- sed
- wget

Note : l'image de conteneur spécialisé pour ce projet inclut l'ensemble de ces outils.

## Exécuter les scripts localement

Note : les outils ligne de commande cités dans la section précédente doivent être installés pour faire fonctionner les scripts.

Utilisez de préférence un environnement virtuel :

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Lancement des scripts :

```bash
./download_and_prepare_data
./process_and_generate
```
