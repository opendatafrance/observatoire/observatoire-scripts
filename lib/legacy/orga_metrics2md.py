#!/usr/bin/env python3
"""
    ODservatoire

    Convert jsonl metrics info file into markdown file
"""
import argparse
from pathlib import Path
import sqlite3
import db_common as dc
import metrics_common as mc


def organizations_nb(cursor):
    """ Query database to get organizations """
    cursor.execute('SELECT COUNT(*) AS nb FROM {ref_org_table}'.format(**dc.SQL_CONSTANTS))
    row = cursor.fetchone()
    return row['nb']


def organizations_type_nb(cursor):
    """ Query database to get organizations types and number"""
    sql = 'SELECT {ref_org_type_col} AS type, COUNT(*) AS nb FROM {ref_org_table} GROUP BY 1 ORDER BY 1'
    cursor.execute(sql.format(**dc.SQL_CONSTANTS))
    for row in cursor.fetchall():
        yield '{} {}'.format(row['nb'], mc.PLURAL_TYPE_MAP.get(row['type'], row['type']))


def main():
    """ Creates markdown doc file from metrics saved in sqlite db """
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('sqlite_db', type=Path, help='SQLite DB file')
    parser.add_argument('md_file', type=Path, help='markdown doc file')
    args = parser.parse_args()

    assert args.sqlite_db.is_file()
    conn = sqlite3.connect(str(args.sqlite_db))
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()
    met_cursor = conn.cursor()

    with args.md_file.open(mode='wt', encoding='utf-8') as md_fd:
        md_fd.write('# Odservatoire - Organisations\n\n')

        md_fd.write("*{} organisations recensées dont:*\n".format(organizations_nb(cursor)))
        for typeorg_str in organizations_type_nb(cursor):
            md_fd.write('- *{}*\n'.format(typeorg_str))
        md_fd.write('\n')

        cursor.execute('SELECT * FROM {ref_org_table}'.format(**dc.SQL_CONSTANTS))
        for row in cursor.fetchall():

            data = {
                'nom': row['nom'],
                'type': row['type'],
                'metrics': mc.compute_metrics_tree(row['siren'], met_cursor)
            }
            mc.write_md_content(data, md_fd)


if __name__ == '__main__':
    main()
