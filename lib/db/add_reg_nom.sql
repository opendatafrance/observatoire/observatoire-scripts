-- Add 'regnom' column to data_orga table
CREATE TEMPORARY TABLE _regions (code TEXT, name TEXT);
INSERT INTO _regions
  SELECT regcode, nom
  FROM data_orga
  WHERE type='REG';
INSERT INTO _regions VALUES ('NOR-28', 'NORMANDIE');
INSERT INTO _regions VALUES ('OCC-76', 'OCCITANIE');
INSERT INTO _regions VALUES ('OM-06', 'OUTREMER');
ALTER TABLE data_orga ADD COLUMN regnom TEXT;
UPDATE data_orga SET regnom=(SELECT name FROM _regions WHERE data_orga.regcode = _regions.code);