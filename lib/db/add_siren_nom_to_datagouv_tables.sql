-- Create temporary table for siren to datagouv id matching
CREATE TEMPORARY TABLE _odf_dg (siren TEXT, nom TEXT, id_data_gouv TEXT);
INSERT INTO _odf_dg
  SELECT siren, nom, id_datagouv
  FROM data_orga
  WHERE id_datagouv != '' AND id_datagouv IS NOT NULL;

-- Use it to update datagouv organizations table
ALTER TABLE data_gouv_organizations ADD COLUMN '_siren';
ALTER TABLE data_gouv_organizations ADD COLUMN '_nom';
UPDATE data_gouv_organizations
SET _siren = (SELECT siren FROM _odf_dg WHERE _odf_dg.id_data_gouv = id),
    _nom = (SELECT nom FROM _odf_dg WHERE _odf_dg.id_data_gouv = id);

-- Use it to update datagouv datasets table
ALTER TABLE data_gouv_datasets ADD COLUMN '_siren';
ALTER TABLE data_gouv_datasets ADD COLUMN '_nom';
UPDATE data_gouv_datasets
SET _siren = (SELECT siren FROM _odf_dg WHERE _odf_dg.id_data_gouv = organization_id),
    _nom = (SELECT nom FROM _odf_dg WHERE _odf_dg.id_data_gouv = organization_id);

-- Use it to update datagouv resources table
ALTER TABLE data_gouv_resources ADD COLUMN '_siren';
ALTER TABLE data_gouv_resources ADD COLUMN '_nom';
UPDATE data_gouv_resources
SET _siren = (SELECT siren FROM _odf_dg WHERE _odf_dg.id_data_gouv = dataset_organization_id),
    _nom = (SELECT nom FROM _odf_dg WHERE _odf_dg.id_data_gouv = dataset_organization_id);
