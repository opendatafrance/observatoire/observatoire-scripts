#!/usr/bin/env python3
import argparse
import collections
import csv
from pathlib import Path
from timeit import default_timer as timer

import geopandas as gpd
import ujson as json
from shapely.geometry import mapping

"""
    Extracts content from an ESRI Shapefile to a CSV file converting coordinates to WGS84
"""

DELIMITER_NAME_TO_CHAR = collections.OrderedDict(
    [("comma", ","), ("semicolon", ";"), ("tab", "\t"),]
)

WGS84_EPSG = "EPSG:4326"


def convert(shp_file: Path, csv_file: Path, csv_delimiter, nb_decimals):
    """ Generates CSV file from Shapefile """

    # Thanks to https://gis.stackexchange.com/questions/188622/rounding-all-coordinates-in-shapely?noredirect=1
    def set_precision(coords, precision):
        result = []
        try:
            return round(coords, int(precision))
        except TypeError:
            for coord in coords:
                result.append(set_precision(coord, precision))
        return result

    # Loads shape file
    gdf = gpd.read_file(shp_file)

    # Converts projection to WGS84
    gdf = gdf.to_crs(WGS84_EPSG)

    # Headers
    headers = [
        colname for colname in gdf.columns.to_list() if colname != "geometry"
    ] + ["GEOMETRY"]
    column_count = len(headers)

    with csv_file.open("wt", encoding="utf-8") as fd:

        writer = csv.writer(
            fd, delimiter=csv_delimiter, quoting=csv.QUOTE_NONE, quotechar=""
        )
        writer.writerow(headers)

        for row in gdf.itertuples():

            # Properties value
            csv_row = [row[i] for i in range(1, column_count)]

            # geometry converted to GeoJSON
            geojson = mapping(row[column_count])
            # Then reduce float precision to 3 digits
            geojson["coordinates"] = set_precision(geojson["coordinates"], nb_decimals)
            # shapely generate geojson with apos, just replace them by quotes
            geojson = str(geojson).replace("'", '"')
            csv_row.append(geojson)

            writer.writerow(csv_row)

    return len(gdf.index)


def main():
    """ Dump SHP file content """
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("shp_file", type=Path, help="ESRI Shapefile to read")
    parser.add_argument("csv_file", type=Path, help="CSV file to write")
    parser.add_argument(
        "--csv_delimiter",
        help="CSV delimiter",
        choices=DELIMITER_NAME_TO_CHAR.keys(),
        default="comma",
    )
    parser.add_argument(
        "--coords_decimals_nb",
        type=int,
        help="decimals nb for geo coordinates",
        default=3,
    )
    args = parser.parse_args()

    assert args.shp_file.is_file()

    print("Converting {} into {}".format(str(args.shp_file), str(args.csv_file)))

    start = timer()
    features_nb = convert(
        args.shp_file,
        args.csv_file,
        DELIMITER_NAME_TO_CHAR[args.csv_delimiter],
        args.coords_decimals_nb,
    )
    end = timer()

    print("Done ({} features exported in {:.2f}s).".format(features_nb, end - start))


if __name__ == "__main__":
    main()
