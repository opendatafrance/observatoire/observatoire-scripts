#!/usr/bin/env python3
import argparse
import csv
import json
import logging
import sys
import time
from pathlib import Path

import requests
import stdnum.fr.siren

API_URL = "https://entreprise.data.gouv.fr/api/sirene/v1/siren/"

log = logging.getLogger(__name__)


def get_coords_by_siren(siren):

    if len(siren) != 9:
        return {
            "status": "err",
            "message": "SIREN code too {}".format(
                "short" if len(siren) < 9 else "long"
            ),
        }
    if not stdnum.fr.siren.is_valid(siren):
        return {"status": "err", "message": "invalid SIREN code"}
    url = API_URL + siren
    req = requests.get(url)
    try:
        json_content = req.json()
    except json.decoder.JSONDecodeError:
        log.warn("Bad JSON content: %s", req.text)
        return {"status": "err", "message": "invalid JSON API response"}
    siege_social = json_content.get("siege_social")
    if siege_social is None:
        return {"status": "err", "message": "SIREN code not found"}

    return {
        "status": "ok",
        "data": {
            "longitude": siege_social.get("longitude", ""),
            "latitude": siege_social.get("latitude", ""),
            "nom": siege_social.get("l1_normalisee", ""),
            "region": siege_social.get("region", ""),
            "departement": siege_social.get("departement", ""),
        },
    }


def main():
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        "input_file", type=Path, help="Text file with one siren number per line"
    )
    parser.add_argument("--out", type=Path, help="Redirect output to a csv file")
    parser.add_argument("--log", default="WARNING", help="level of logging messages")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: {}".format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,
    )  # Use stderr if script outputs data to stdout.

    input_filepath = args.input_file
    if not input_filepath.exists():
        parser.error("Can't read %s", str(input_filepath))

    out = args.out.open("wt", encoding="utf-8") if args.out else sys.stdout

    writer = csv.writer(out)
    writer.writerow(
        [
            "siren",
            "nom",
            "latitude",
            "longitude",
            "code_region",
            "code_departement",
            "statut",
            "message",
        ]
    )

    with input_filepath.open("rt", encoding="utf-8") as fd:
        for line in fd:
            siren = line.strip()
            if siren:
                # delay to avoid 429 "Too many requests"
                time.sleep(0.2)

                res = get_coords_by_siren(siren)
                if res["status"] == "err":
                    writer.writerow(
                        [siren, "", "", "", "", "", "", "err", res["message"]]
                    )
                    continue

                data = res.get("data")
                writer.writerow(
                    [
                        siren,
                        data["nom"],
                        data["latitude"],
                        data["longitude"],
                        data["region"],
                        data["departement"],
                        "ok",
                        "",
                    ]
                )

    if args.out:
        out.close()


if __name__ == "__main__":
    sys.exit(main())
