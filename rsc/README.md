# Référentiels utilisés

Afin de [produire les données l'Observatoire open data des territoires](https://git.opendatafrance.net/observatoire/observatoire-data/).

### API Sirene

- Fournisseur : [Etalab](https://entreprise.data.gouv.fr/api_doc_sirene)
- Millésime : v1
- Utilisation : latitude et longitude d'un établissement identifié par son SIREN
- Détail :
  - géocodage de l'ensemble des établissements [référencés par OpenDataFrance](https://git.opendatafrance.net/observatoire/observatoire-data/-/tree/master#donn%C3%A9es-sources)
  - utilisé dans les GeoJSON produits pour représenter par un point les établissements de type AGCT, COM, DSPT et OACT

### [ADMIN-EXPRESS-COG](/ADE_1-1_SHP_LAMB93_FR)

- Fournisseur : [Institut géographique national (IGN)](https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#admin-express)
- Millésime : 2018 (version 1.1)
- Format fichier : shapefile
- Utilisation : contours d'un établissement identifié par son code Insee ou son SIREN
- Détail :
  - `COMMUNE.shp` : utilisé uniquement pour Paris (représenté par un contour plutôt qu'un point comme les autres communes)
  - `DEPARTEMENT.shp` : utilisé pour les établissements de type département, identifiés par leur code Insee
  - `EPCI.shp` : utilisé pour les EPCI (CA, CC, CU et MET), identifiés par leur SIREN
  - `REGION.shp` : utilisé pour les établissements de type région, identifiés par leur code Insee

### [Code officiel géographique (COG)](/INSEE-COG)

- Fournisseur : [Institut national de la statistique et des études économiques (Insee)](https://www.insee.fr/fr/information/2560452)
- Millésime : 2019
- Format fichier : CSV
- Utilisation : référentiel des départements et régions
- Détail :
  - `departement2019.csv` : obtention du nom d'un département à partir de son code Insee
  - `region2019.csv` : obtention du nom d'une région à partir de son code Insee

### [Intercommunalités de la Région Île-de-France](/IAU-IDF)

- Fournisseur : [Institut Paris Région](https://data-iau-idf.opendata.arcgis.com/) (anciennement Institut d'aménagement et d'urbanisme Île-de-France, IAU-IDF)
- Millésime : 2018
- Format fichier : shapefile
- Utilisation : contours des intercommunalités de la Région Île-de-France
- Détail :
  - `Intercommunalites_de_la_Region_IledeFrance_au_1er_janvier_2018.shp` : utilisé pour pour les organisations de type EPT
