#!/usr/bin/env python3
"""
    Adds geometry information to orga_metrics table
"""
import argparse
from pathlib import Path
import sqlite3
import db_common as dc


def add_region_geometry(cursor):
    """ Adds geometry for 'REG' type """
    sql = """UPDATE {orga_geometry_table}
             SET {orga_geometry_coords_col} = (SELECT geometry
                                FROM georef.ae_metropole_region
                                WHERE INSEE_REG = {orga_geometry_table}.{orga_geometry_code_col})
             ,   {orga_geometry_source_col} = (SELECT 'AdminExpress'
                                FROM georef.ae_metropole_region
                                WHERE INSEE_REG = {orga_geometry_table}.{orga_geometry_code_col})
             WHERE type = 'REG'
          """.format(**dc.SQL_CONSTANTS)
    cursor.execute(sql)


def add_dept_geometry(cursor):
    """ Adds geometry for 'DEP' type """
    sql = """UPDATE {orga_geometry_table}
             SET {orga_geometry_coords_col} = (SELECT geometry
                                FROM georef.ae_metropole_departement
                                WHERE REPLACE(NOM_DEP, '-', ' ') = {orga_geometry_table}.{ref_org_name_col})
             ,   {orga_geometry_source_col} = (SELECT 'AdminExpress'
                                FROM georef.ae_metropole_departement
                                WHERE REPLACE(NOM_DEP, '-', ' ') = {orga_geometry_table}.{ref_org_name_col})
             WHERE type = 'DEP'
          """.format(**dc.SQL_CONSTANTS)
    cursor.execute(sql)


def add_paris_polygon_geometry(cursor):
    """ Adds polygon geometry for Paris """
    sql = """UPDATE {orga_geometry_table}
             SET {orga_geometry_coords_col} = (SELECT geometry
                                FROM georef.ae_metropole_commune
                                WHERE REPLACE(NOM_COM_M, '-', ' ') = {orga_geometry_table}.{orga_geometry_nom_col}
                                AND INSEE_REG = {orga_geometry_table}.{orga_geometry_code_col})
             ,   {orga_geometry_source_col} = (SELECT 'AdminExpress'
                                FROM georef.ae_metropole_commune
                                WHERE REPLACE(NOM_COM_M, '-', ' ') = {orga_geometry_table}.{orga_geometry_nom_col}
                                AND INSEE_REG = {orga_geometry_table}.{orga_geometry_code_col})
             WHERE type = 'COM' and nom = 'PARIS'
          """.format(**dc.SQL_CONSTANTS)
    cursor.execute(sql)


def add_epci_geometry(cursor):
    """ Adds geometry for 'CA', 'CC', 'CU' and 'MET' type """
    sql = """UPDATE {orga_geometry_table}
             SET {orga_geometry_coords_col} = (SELECT geometry
                                FROM georef.ae_metropole_epci
                                WHERE CODE_EPCI = {orga_geometry_table}.{orga_geometry_siren_col})
             , {orga_geometry_source_col} = (SELECT 'AdminExpress'
                                FROM georef.ae_metropole_epci
                                WHERE CODE_EPCI = {orga_geometry_table}.{orga_geometry_siren_col})
             WHERE type in ('CA', 'CC', 'CU', 'MET')
          """.format(**dc.SQL_CONSTANTS)
    cursor.execute(sql)


def add_ept_geometry(cursor):
    """ Adds geometry for 'EPT' from IAU-IDF """
    sql = """UPDATE {orga_geometry_table}
             SET {orga_geometry_coords_col} = (SELECT geometry
                                FROM georef.iau_ept
                                WHERE SIREN = {orga_geometry_table}.{orga_geometry_siren_col})
            , {orga_geometry_source_col} = (SELECT 'IAU IDF'
                                FROM georef.iau_ept
                                WHERE SIREN = {orga_geometry_table}.{orga_geometry_siren_col})
             WHERE type = 'EPT'
          """.format(**dc.SQL_CONSTANTS)
    cursor.execute(sql)


def create_points_temporary_table(cursor):
    """ Create temporary table _geopoints containing points if possible when lat, long are non empty """

    sql = """CREATE TEMPORARY TABLE _geopoints (siren TEXT, lat TEXT, lng TEXT, coords TEXT)"""
    cursor.execute(sql)

    sql = """INSERT INTO _geopoints
             SELECT {odf_org_siren_col}, {odf_org_lat_col}, {odf_org_long_col}, ''
             FROM {odf_org_table}""".format(**dc.SQL_CONSTANTS)
    cursor.execute(sql)

    sql = """UPDATE _geopoints
             SET coords = '{"type": "Point", "coordinates": [' || lng || ', ' || lat || ']}'
             WHERE lng != '' AND lat != ''"""
    cursor.execute(sql)


def add_point_geometry(cursor, types=None):
    """ Adds point geometry where needed """

    type_cond = '1' if not types else 'type in ({})'.format(', '.join(["'{}'".format(t) for t in types]))

    sql = """UPDATE {orga_geometry_table}
             SET {orga_geometry_coords_col} = (SELECT coords
                                FROM _geopoints
                                WHERE siren = {orga_geometry_table}.{orga_geometry_siren_col})
             ,   {orga_geometry_source_col} = (SELECT 'OpenDataFrance'
                                FROM {odf_org_table}
                                WHERE siren = {orga_geometry_table}.{orga_geometry_siren_col})
             WHERE TYPE_COND AND ({orga_geometry_coords_col} = '' OR {orga_geometry_coords_col} IS NULL) 
          """.format(**dc.SQL_CONSTANTS)
    sql = sql.replace('TYPE_COND', type_cond)
    cursor.execute(sql)


def warn_not_found_geometry(cursor):
    """ Warn if some geometry is missing """
    sql = """SELECT {odf_org_table}.{odf_org_siren_col} AS siren,
                    {odf_org_table}.{odf_org_nom_col} AS nom,
                    {odf_org_lat_col}, {odf_org_long_col}
             FROM {odf_org_table}, {orga_geometry_table}
             WHERE {odf_org_table}.{odf_org_siren_col} = {orga_geometry_table}.{orga_geometry_siren_col}
               AND (   {orga_geometry_table}.{orga_geometry_coords_col} == ''
                    OR {orga_geometry_table}.{orga_geometry_coords_col} IS NULL)
          """.format(**dc.SQL_CONSTANTS)
    cursor.execute(sql)
    for row in cursor.fetchall():
        addon_info = '\n=> les informations lat, long seront utilisées' if row[
            'lat'] != '' and row['long'] != '' else "\n=> Pas d'information lat,long de secours !"
        info = {
            'nom': row['nom'],
            'siren': row['siren'],
            'addon': addon_info,
        }
        print('Géométrie non trouvée pour "{nom}" (siren {siren}){addon}'.format(**info))


def populate_geo_data(cursor, georefdb):
    """ Creates geometry column and fills it using data in georefdb """

    # Creates table orga_geometry from ref_org table
    cursor.execute('DROP TABLE IF EXISTS {orga_geometry_table}'.format(**dc.SQL_CONSTANTS))
    cursor.execute('CREATE TABLE {orga_geometry_table} AS SELECT * FROM {ref_org_table}'.format(**dc.SQL_CONSTANTS))

    # Addds colum "code"
    cursor.execute('ALTER TABLE {orga_geometry_table} ADD COLUMN {orga_geometry_code_col} TEXT'
                   .format(**dc.SQL_CONSTANTS))
    cursor.execute("""UPDATE {orga_geometry_table}
             SET {orga_geometry_code_col} =
                    (SELECT SUBSTR({odf_org_regcode_col}, INSTR({odf_org_regcode_col},'-')+1)
                     FROM {odf_org_table}
                     WHERE {odf_org_siren_col} = {orga_geometry_table}.{orga_geometry_siren_col})
                    """.format(**dc.SQL_CONSTANTS))

    # Adds column "source"
    cursor.execute('ALTER TABLE {orga_geometry_table} ADD COLUMN {orga_geometry_source_col} TEXT'
                   .format(**dc.SQL_CONSTANTS))

    # Adds geometry column
    cursor.execute('ALTER TABLE {orga_geometry_table} ADD COLUMN {orga_geometry_coords_col} TEXT'
                   .format(**dc.SQL_CONSTANTS))

    # Attach georef database
    cursor.execute("ATTACH DATABASE '{}' AS georef".format(str(georefdb)))

    # Adds REGION geometry (join on regcode)
    add_region_geometry(cursor)

    # Adds DEPT geometry (join on name...)
    add_dept_geometry(cursor)

    # Adds CA, CC, CU and MET geometry (join on siren)
    add_epci_geometry(cursor)

    # Adds EPT geometry (join on siren)
    add_ept_geometry(cursor)

    # Exception for Paris (use polygon instead of points)
    add_paris_polygon_geometry(cursor)

    # Creates temporary table for geopoints
    create_points_temporary_table(cursor)

    # Adds OACT, AGCT, COM and DSPT geometry (join on siren) from OpenDataFrance lat, long info
    add_point_geometry(cursor, ('OACT', 'AGCT', 'COM', 'DSPT'))

    # Warn if geometry not found
    warn_not_found_geometry(cursor)

    # Fill missing geometry with points
    add_point_geometry(cursor)


def main():
    """ Adds geometry information to orga_metrics """
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("db_file", type=Path, help="SQLite DB file")
    parser.add_argument("georef_db_file", type=Path, help="SQLite Geo ref DB file")

    args = parser.parse_args()

    assert args.db_file.exists()
    assert args.georef_db_file.exists()

    conn = sqlite3.connect(str(args.db_file))
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()

    populate_geo_data(cur, args.georef_db_file)

    conn.commit()
    conn.close()


if __name__ == '__main__':
    main()
