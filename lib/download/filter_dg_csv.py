#!/usr/bin/env python3
"""
    Filter DataGouv dumps to retain only organizations referenced by OpenDataFrance
"""
import argparse
import csv
import sys
from pathlib import Path

CSV_DELIMITER = ';'


def main():
    """ Filters CSV file content to retain only data related to organizations in ODF GoogleSheet """
    parser = argparse.ArgumentParser()
    parser.add_argument('orgid_text_file', type=Path, help='text file containing datagouv ids to retain')
    parser.add_argument('orgid_col_name', help='Column name of organization id in source file')
    parser.add_argument('csv_input_file', type=Path, help='Source CSV file')
    parser.add_argument('csv_output_file', type=Path, help='Filtered CSV file')
    args = parser.parse_args()

    if not args.orgid_text_file.is_file():
        parser.error('Organizations id text file [{}] not found'.format(str(args.orgid_text_file)))
    orgid_text_file = args.orgid_text_file
    if not args.csv_input_file.is_file():
        parser.error('CSV input file [{}] not found'.format(str(args.csv_input_file)))
    csv_input_file = args.csv_input_file
    csv_output_file = args.csv_output_file
    orgid_col_name = args.orgid_col_name

    # Computes organisation ids set
    orgid_set = set()
    with orgid_text_file.open("rt", encoding='ascii') as fd:
        for line in fd:
            line = line.strip()
            if line:
                orgid_set.add(line)

    # To avoid '_csv.Error: field larger than field limit (131072)' error
    csv.field_size_limit(sys.maxsize)

    # Reads Input CSV
    with csv_input_file.open('rt', encoding='utf-8') as fd:

        # First gets header line to have cols in original order
        header_line = next(fd).strip()
        headers = [val.strip('"') for val in header_line.split(CSV_DELIMITER)]
        if orgid_col_name not in headers:
            print('Unknown column [{}] in {}'.format(orgid_col_name, csv_input_file.name))
            sys.exit(1)
        orgid_col_idx = headers.index(orgid_col_name)

        # Reset to beginning of file
        fd.seek(0)

        # Filter CSV file
        with csv_output_file.open('wt', encoding='utf-8') as fdout:

            writer = csv.writer(fdout, delimiter=CSV_DELIMITER)
            writer.writerow(headers)

            reader = csv.reader(fd, delimiter=CSV_DELIMITER)
            for row in reader:
                # filter row
                if orgid_col_idx >= len(row) or row[orgid_col_idx] not in orgid_set:
                    continue
                writer.writerow(row)


if __name__ == '__main__':
    main()
