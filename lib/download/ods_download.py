#!/usr/bin/env python3
"""
    Download dumps (catalog and monitoring) from opendatasoft platforms described in given csv file
"""
import argparse
import csv
import logging
import sys
import urllib
from pathlib import Path

import requests
import ujson as json
import urllib3
from slugify import slugify

CATALOG_URL_TPL = '{}/api/v2/catalog/exports/csv'
MONITORING_URL_TPL = '{}/api/v2/monitoring/datasets/ods-datasets-monitoring/exports/csv'

LOG = logging.Logger('ods_download')


def get_organisation_infos(csv_file: Path):
    """Reads CSV file and return (yield) organizations"""
    with csv_file.open(mode='rt', encoding='utf-8') as fd:
        reader = csv.DictReader(fd, delimiter=',')
        for row in reader:
            if row.get('id-ods'):
                yield row


def compute_base_url(url):
    """Converts https://data.rennesmetropole.fr/explore/?sort=modified&refine.publisher=Ville+de+Rennes
        into https://data.rennesmetropole.fr"""
    return "{0.scheme}://{0.netloc}".format(urllib.parse.urlsplit(url))


def compute_publisher_query(publisher):
    """Compute query relating to publisher
    >>> compute_publisher_query('Ville de Rennes')
    'publisher:"Ville de Rennes"'
    >>> compute_publisher_query('Mairie de Paris*')
    'publisher LIKE "Mairie de Paris"'
    >>> compute_publisher_query('Grand Paris Sud + Numérique @ Grand Paris Sud + sig@grand paris sud*')
    'publisher:"Grand Paris Sud" OR publisher:"Numérique @ Grand Paris Sud" OR publisher LIKE "sig@grand paris sud"'
    """
    parts = []
    for pub in publisher.split(' + '):
        if pub[-1] == '*':
            parts.append('publisher LIKE "{}"'.format(pub[:-1]))
        else:
            parts.append('publisher:"{}"'.format(pub))
    return ' OR '.join(parts)


def download_and_save(url, file_path: Path, file_prefix, cache=True):
    """Downloads an url ignoring ssl cert error"""
    log.debug("Downloading %s... ", file_path.stem)
    if cache and file_path.exists():
        log.debug('-> cached.')
        return
    try:
        req = requests.get(url, verify=False)
    except requests.ConnectionError as err:
        log.warning("Erreur de connexion à %s\n=> %s", url, err)
        return
    if not req.ok:
        log.warning("Retour HTTP invalide de %s\n=> %s", url, req.status_code)
        return
    content_type = req.headers['content-type']
    if not '/csv' in content_type:
        log.warning("Données non disponibles pour : {}".format(file_prefix))
        return

    with file_path.open(mode='wt', encoding='utf-8') as fdout:
        fdout.write(req.text)
    log.debug('-> done')


log = logging.getLogger(__name__)


def main():
    """Reads OpenDataFrance CSV file and downloads dumps"""
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('odf_csv_file', type=Path, help='OpenDataFrance CSV file')
    parser.add_argument('target_dir', type=Path, help='Target dir for ods dumps')
    args = parser.parse_args()

    if not args.odf_csv_file.exists():
        parser.error("Can't find [{}] CSV input file".format(str(args.odf_csv_file)))
    csv_file = args.odf_csv_file
    if not args.target_dir.exists():
        parser.error("Can't find [{}] target directory".format(str(args.target_dir)))
    target_dir = args.target_dir

    # Suppress warnings about bad SSL certs
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    for org_info in get_organisation_infos(csv_file):

        # Base URL is found in "url-ptf" column
        url = org_info['url-ptf']
        base_url = compute_base_url(url)

        # Compose URL
        publisher_query = compute_publisher_query(org_info['id-ods'])
        params = {
            'delimiter': ',',
            'where': publisher_query
        }
        param_str = '?' + '&'.join(['{}={}'.format(k, urllib.parse.quote_plus(v)) for k, v in params.items()])

        file_prefix = '{}_{}'.format(org_info['siren'], slugify(org_info['nom']))

        cat_url = CATALOG_URL_TPL.format(base_url) + param_str
        download_and_save(cat_url, target_dir / '{}_catalog.csv'.format(file_prefix), file_prefix)

        mon_url = MONITORING_URL_TPL.format(base_url) + param_str
        download_and_save(mon_url, target_dir / '{}_monitoring.csv'.format(file_prefix), file_prefix)
    return 0


if __name__ == '__main__':
    sys.exit(main())
